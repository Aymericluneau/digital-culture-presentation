
/**
 * Script for 2FA Authy.
 *
 *  @param {jQuery} $        jQuery object.
 *  @param {Object} window   The window object.
 */


(function ($, window) {

	jQuery(document).on('click', '[data-check-authy-2fa]', function () {
		const key = jQuery('#2fa-authy-api').val();
		const ajaxURL = (typeof wp2faWizardData != "undefined") ? wp2faWizardData.ajaxURL : ajaxurl;
		const nonceValue = jQuery(this).attr('data-nonce');

		jQuery.ajax({
			url: ajaxURL,
			data: {
				action: 'check_authy_api_key',
				authy_key: key,
				_wpnonce: nonceValue
			},
			success: function (data) {
				if (data.success) {
					if (jQuery('input#authy').length) {
						storeKey(ajaxURL, key, nonceValue);
					} else {
						displayDialog(wp2faAuthy.keyCheckSuccessful);
					}
				} else {
					displayDialog(data.data);
				}
			},
		});
	});

	function displayDialog(msg) {
		jQuery('<div></div>').dialog({
			modal: true,
			title: wp2faAuthy.dialogTitle,
			dialogClass: "alert",
			open: function() {
			  jQuery(this).html(msg);
			},
			buttons: {
			  OK: function() {
				$( this ).dialog( "close" );
			  }
			}
		});
		jQuery(".ui-dialog").css({borderRadius: "5px"});
	}

	jQuery(document).on('click', '[data-api-setup-wizard]', function (e) {
		e.preventDefault();
		jQuery('#wizard-api-key').slideDown( 600 );
	});

	var iti = jQuery("#wp-2fa-phone").intlTelInput({
		initialCountry: "auto",
		geoIpLookup: function (callback) {
			jQuery.get("http://ipinfo.io", function () { }, "jsonp").always(function (resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		placeholderNumberType: "MOBILE",
		utilsScript: wp2faAuthy.utilsUrl,
	});

	// Checks and validates the given telephone number
	jQuery(document).on('click', '#authy_tel', function (e) {
		e.preventDefault();
		const errorWrapper = jQuery('#error-msg');
		errorWrapper.addClass('hidden');

		if ('' === jQuery('#wp-2fa-phone').val().trim()) {
			errorWrapper.html('No number provided');
			errorWrapper.removeClass('hidden');
		} else {
			if (!iti.intlTelInput("isValidNumber")) {
				errorWrapper.html(wp2faAuthy.errorMap[iti.intlTelInput("getValidationError")]);
				errorWrapper.removeClass('hidden');
			} else {
				var fullNumber = iti.intlTelInput("getNumber");
				var countryCode = iti.intlTelInput('getSelectedCountryData').dialCode;
				var phone = fullNumber.replace("+" + countryCode, '');

				registerUser(countryCode, phone, jQuery(this).data('nonce'));
			}
		}
	});

	/**
	 * Responds with error
	 * 
	 * @param {string} text 
	 */
	function displayError(text) {
		if (jQuery('#error-msg').length) {
			const errorWrapper = jQuery('#error-msg');
			errorWrapper.html(text);
			errorWrapper.removeClass('hidden');
		}
	}

	/**
	 * Registers Authy user.
	 * 
	 * @param {string} countryCode 
	 * @param {string} phone 
	 */
	function registerUser(countryCode, phone, nonce) {
		const ajaxURL = (typeof wp2faWizardData != "undefined") ? wp2faWizardData.ajaxURL : ajaxurl;
		jQuery.ajax({
			url: ajaxURL,
			data: {
				action: 'register_user',
				country_code: countryCode,
				phone: phone,
				_wpnonce: nonce
			},
			success: function (data) {
				if (data.success) {
					const currentSubStep = jQuery('.authy-step-setting-wrapper');
					const nextSubStep = jQuery(currentSubStep).nextAll('.authy-step-setting-wrapper:not(.active)').filter(':first');
					jQuery(currentSubStep).removeClass('active');
					jQuery(currentSubStep).addClass('hidden');
					jQuery(nextSubStep).addClass('active');
					jQuery(nextSubStep).removeClass('hidden');
				} else {
					displayError(data.data);
				}
			},
			error: function (data) {
				displayError(data.responseJSON.data[0].message);
			}
		});
	}

	/**
	 * Stores the provided Authy API key
	 * 
	 * @param {string} ajaxURL 
	 * @param {string} key 
	 */
	function storeKey(ajaxURL, key, nonce) {
		jQuery.ajax({
			url: ajaxURL,
			data: {
				action: 'store_authy_api_key',
				authy_api_key: key,
				_wpnonce: nonce
			},
			success: function (data) {
				if (data.success) {
					jQuery('#wizard-api-key').fadeOut(1000);
					jQuery('input#authy').removeClass('disabled').removeAttr('data-api-setup-wizard').prop('checked', true);
				} else {
					alert(data.data);
				}
			},
		});
	}

	/**
	 * Shows verification messages
	 * 
	 * @param {string} type 
	 * @param {string} text 
	 */
	function verificationResponse(type, text) {
		let wrapper = jQuery('.verification-response-authy');

		if ('error' === type) {
			wrapper.html('<span style="color:red">' + text + '</span>');
		} else {
			wrapper.html('<span style="color:green">' + text + '</span>');
		}
		wrapper.removeClass('hidden');
	}

	// Authy validation button
	jQuery(document).on('click', 'a[data-validate-authy-ajax]', function (e) {
		e.preventDefault();

		tokenField = jQuery('#authy-token');
		if ('' === tokenField.val().trim()) {
			verificationResponse('error', 'No token provided');
		} else {
			let isValid = false;
			let regex = /^[0-9\s]*$/;
			isValid = regex.test(tokenField.val());
			if (!isValid) {
				verificationResponse('error', 'Invalid token format');
			} else {
				let token = tokenField.val();
				let nonce = jQuery(this).data('nonce');

				checkToken(token, nonce);
			}
		}
	});

	/**
	 * Checks token against the Authy API
	 * 
	 * @param {string} token 
	 * @param {string} nonce 
	 */
	function checkToken(token, nonce) {
		const ajaxURL = (typeof wp2faWizardData != "undefined") ? wp2faWizardData.ajaxURL : ajaxurl;
		let closeButton = jQuery('button[data-close-2fa-modal');
		let setMethod = (closeButton.length) ? true : false;

		jQuery.ajax({
			url: ajaxURL,
			data: {
				action: 'check_authy_token',
				token: token,
				method: setMethod,
				_wpnonce: nonce
			},
			success: function (data) {
				if (data.success) {
					if (jQuery('a[data-validate-authy-ajax]').length) {
						// Called from the profile setup - close the wizard
						let nextSubStep = jQuery( '#2fa-wizard-config-backup-codes' );
						jQuery( this ).parent().parent().find( '.active' ).not( '.step-setting-wrapper' ).removeClass( 'active' );
						jQuery( '.wizard-step.active' ).removeClass( 'active' );
						jQuery( nextSubStep ).addClass( 'active' );
	
						jQuery( document ).on( 'click', '#select-backup-method', function( e ) {
							e.preventDefault();
							var backupRadio = jQuery("input[name=backup_method_select]:checked");
	
							jQuery( '.wizard-step.active' ).removeClass( 'active' );
							jQuery( '#'+backupRadio.data('step') ).addClass( 'active' );
						} );
	
						jQuery( document ).on( 'click', '[name="save_step"], [data-close-2fa-modal]', function() {
							if ( 'redirectToUrl' in wp2faWizardData &&
									'' != jQuery.trim( wp2faWizardData.redirectToUrl ) ) {
								window.location.replace( wp2faWizardData.redirectToUrl );
							} else {
								removeShowParam();
							}
						} );
					}
				} else {
					alert(data.data);
				}
			},
			error: function (data) {
				verificationResponse('error', data.responseJSON.data[0].message);
			}
		});

	}

	var timeOutId = '';

	jQuery("#authy-onetouch-btn").on("click", function () {
		jQuery("#wp-2fa-authy-onetouch").css('display', 'none');
		jQuery("#wp-2fa-authy").css('display', 'inline-block');
		jQuery("#submit").css('display', 'inline-block');
		clearTimeout(timeOutId);
	});

	jQuery(function () {
		var storiesInterval = 2000;

		if (jQuery('#wp-2fa-authy-onetouch').length && jQuery('#wp-2fa-authy-onetouch').css('display') !== 'none') {
			var authyCheck = function () {
				jQuery.ajax({
					type: "GET",
					url: wp2faAuthy.onetouchAjax,
					data: {
						action: 'onetouch_status',
						one_touch: jQuery('#onetouch_uuid').val(),
						user: jQuery('#wp-auth-id').val(),
						_wpnonce: jQuery('#wp-auth-nonce').val(),
						redirect: jQuery('input[name=redirect_to]').val(),
						rememberme: jQuery('#rememberme').val()
					}
				}).success(function (data) {
					if (data.data !== undefined && data.data.redirect_to !== undefined) {
						clearInterval(timeOutId);
						let wp_auth_check_wrap = jQuery( '#wp-auth-check-wrap', window.parent.document );
						if ( wp_auth_check_wrap && wp_auth_check_wrap.is( ':visible' ) ) {
							jQuery( 'body', window.parent.document ).removeClass( 'modal-open' );
							wp_auth_check_wrap.addClass('hidden').css('display', '');
							jQuery( '#wp-auth-check-frame', window.parent.document ).remove();
							window.onbeforeunload = null;
							jQuery(window).off('beforeunload');
							wp_auth_check_wrap.find( '.wp-auth-check-close' ).click();
						} else {
							window.location.replace(data.data.redirect_to);
						}
					}
				}).fail(function () {
					console.log(data.responseJSON.data[0].message);
				}).always(function () {
					
				});
			}

			timeOutId = setInterval(authyCheck, storiesInterval);
		}
	});

}(jQuery, window));
