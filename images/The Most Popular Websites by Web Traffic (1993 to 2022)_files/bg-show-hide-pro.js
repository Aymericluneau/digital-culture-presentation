if ('undefined' === typeof BG_SHCE_PRO_USE_EFFECTS) {
	BG_SHCE_PRO_USE_EFFECTS = '0';
} 
if ('undefined' === typeof BG_SHCE_PRO_TOGGLE_SPEED) {
	BG_SHCE_PRO_TOGGLE_SPEED = '0';
} 
if ('undefined' === typeof BG_SHCE_PRO_TOGGLE_OPTIONS) {
	BG_SHCE_PRO_TOGGLE_OPTIONS = 'swing';
}
if ('undefined' === typeof BG_SHCE_PRO_TOGGLE_EFFECT) {
	BG_SHCE_PRO_TOGGLE_EFFECT = '';
}

if ('undefined' === typeof BG_SHCE_PRO_ACCORDION) {
	BG_SHCE_PRO_ACCORDION = '0';
}


function bgExpandCollapsedContentPro() {
	var bgCollapseExpandItems = document.querySelectorAll('input[bg_collapse_expand_pro]');
	var dynCss = '';
	
	for ( i=0; i<bgCollapseExpandItems.length; i++) {
		
		var showHideButton = document.getElementById('bg-showmore-action-'+bgCollapseExpandItems[i].value);	
		var hiddenContent = document.getElementById('bg-showmore-pro-hidden-'+bgCollapseExpandItems[i].value);
		var bgColorOfIcon = document.getElementById('bg-icon-bg-color-'+bgCollapseExpandItems[i].value).value;
		
		if (showHideButton && hiddenContent) {
			if (window.addEventListener) {
				showHideButton.addEventListener('click', function(event) {bgExpandCollapsedContentActionPro(event, this);} );
			}
			else {
				window.attachEvent("onclick", function(event) {bgExpandCollapsedContentActionPro(event, this);} );
			}
			if (bgColorOfIcon) {
				dynCss += '#bg-showmore-action-'+bgCollapseExpandItems[i].value+':before { background:'+bgColorOfIcon+';color:white;}';
			}
		}
		
		if (jQuery( showHideButton ).parent().prop("tagName") === 'LI' && 
			jQuery( showHideButton ).parent().parent().prop("tagName") === 'UL') {
			jQuery( showHideButton ).parent().parent().css('margin-bottom',0);
		}
				
	}
	
	var	head = document.head || document.getElementsByTagName('head')[0];
	var	style = document.createElement('style');

	style.type = 'text/css';
	if (style.styleSheet){
	  style.styleSheet.cssText = dynCss;
	} else {
	  style.appendChild(document.createTextNode(dynCss));
	}

	head.appendChild(style);

	bgToggleBasedOnUrlParam();

}

/* FE */
function bgExpandCollapsedContentActionPro(event, thisObj, group) {
	event.preventDefault();
	if (!group) {
		group = false;
	}
	
	var bgUniqId = thisObj.id.replace('bg-showmore-action-', '');
		
	var showHideButton = document.getElementById('bg-showmore-action-'+bgUniqId);	
	var hiddenContent = document.getElementById('bg-showmore-pro-hidden-'+bgUniqId);
	
	var showLessText = document.getElementById('bg-show-less-text-'+bgUniqId) ? document.getElementById('bg-show-less-text-'+bgUniqId).value : '';
	var showMoreText = document.getElementById('bg-show-more-text-'+bgUniqId) ? document.getElementById('bg-show-more-text-'+bgUniqId).value : '';
	
	var text = jQuery(showHideButton).text();
	if (BG_SHCE_PRO_USE_EFFECTS === '1') {
		jQuery( hiddenContent ).toggle(BG_SHCE_PRO_TOGGLE_EFFECT, BG_SHCE_PRO_TOGGLE_OPTIONS, parseInt(BG_SHCE_PRO_TOGGLE_SPEED)+1);
	}
	else {
		jQuery( hiddenContent ).toggle(parseInt(BG_SHCE_PRO_TOGGLE_SPEED));

	}
	
	if (showLessText === '') {
		jQuery( showHideButton ).toggle();
		
		if (jQuery( showHideButton ).parent().prop("tagName") === 'LI') {
			jQuery( showHideButton ).parent().toggle();
		}
	}
	else {
		jQuery(showHideButton).text(
			 text == showMoreText ? showLessText : showMoreText
		);
		jQuery(showHideButton).toggleClass("bg-close");
	}
	
	var bgCollapseExpandItems = document.querySelectorAll('input[bg_collapse_expand_pro]');
	if (BG_SHCE_PRO_ACCORDION === "1" && group != true) {
		var bgCollapseExpandItems = bgGetSiblings(thisObj);
		for ( i=0; i<bgCollapseExpandItems.length; i++) {
			if ( bgCollapseExpandItems[i].value != bgUniqId) {
				jQuery( '#bg-showmore-pro-hidden-'+bgCollapseExpandItems[i].value ).hide(parseInt(BG_SHCE_PRO_TOGGLE_SPEED));
				jQuery( '#bg-showmore-action-'+bgCollapseExpandItems[i].value ).removeClass("bg-close");
				jQuery( '#bg-showmore-action-'+bgCollapseExpandItems[i].value ).text(jQuery('#bg-show-more-text-'+bgCollapseExpandItems[i].value).val());
			}
		}
	}

	if ('undefined' !== typeof gmspAllMaps && 'undefined' !== typeof gmspResizeMaps) {
		gmspResizeMaps('recenterHidden');
	}
}

function bgCollapseExpandGroup() {
	var group = jQuery(".bg-shce-group");
	if (!group.length) {
		return;
	}
			
	group.click(function(event) {
		var clickedItemClasses = jQuery(this).attr("class").split(' ');
		var clickedItemGroupNumIndex = jQuery.inArray('bg-shce-group', clickedItemClasses)+1;
		
		for (var i=0; i<group.length; i++) {
			if (group[i] != this && jQuery(group[i]).hasClass(clickedItemClasses[clickedItemGroupNumIndex])) {
				if (jQuery(this).hasClass('bg-close')) {
					bgExpandCollapsedContentActionPro(event, group[i], true);
				} else {
					bgExpandCollapsedContentActionPro(event, group[i], false);				
				}
			}
		}
		
	});
	
}

function bgGetSiblings(el) {
	var siblings = [];
	par = jQuery(document).find('input[bg_collapse_expand_pro]');
	for (var i=0; i<par.length; i++) {
		siblings.push(par[i]);
	}
    return siblings;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function bgToggleBasedOnUrlParam() {
	var urlParams = getUrlVars();
	if (urlParams.hasOwnProperty('state') && urlParams.hasOwnProperty('id')) {
	 	var customId = urlParams.id;
		var customState = urlParams.state;
		var el = document.getElementById("bg-showmore-action-"+customId);
		el.scrollIntoView();
		if (customState == "open") {
			jQuery("div#bg-showmore-pro-hidden-"+customId).addClass("bg-showmore-open");
			jQuery("span#bg-showmore-pro-hidden-"+customId).addClass("bg-showmore-open");
			jQuery("#bg-showmore-action-"+customId).addClass("bg-close");
			jQuery("#bg-showmore-action-"+customId).text(jQuery('#bg-show-less-text-'+customId).val());
			
		}
	}

	
}

/* BE */
function bgSelectedEffectPro() {
	var effectOpts = jQuery('#bg_shce_effect > option');
	if (effectOpts.length > 0) {
		var i = 0;
		while ( i < effectOpts.length ) {
			if (effectOpts[i].value === BG_SHCE_PRO_TOGGLE_EFFECT) {
				effectOpts[i].selected = true;
			}
			i++;
		}
	}
	
	if (BG_SHCE_PRO_USE_EFFECTS === '1') {
		jQuery('.bg-effects').show();
	} 
}

jQuery(document).ready(function() {
	bgExpandCollapsedContentPro();
	bgSelectedEffectPro();
	bgCollapseExpandGroup();
});