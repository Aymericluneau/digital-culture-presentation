---
title: "Network_analysis"
author: "aymeric"
date: '2022-08-02'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
 echo = FALSE,
 message = FALSE,
 warning = FALSE,
 fig.height=8,
 fig.width=10
 #fig.align = "center"
)
options(encoding = 'UTF-8')


library(lsa)
library("tidyverse")
library("igraph")
library("questionr")
library("ggpubr")
library("tidytext")
library("knitr")
library("tidygraph")
library("ggraph")
library("stringr")
library("bookdown")
library(stopwords) # Mots "inutiles"
library(SnowballC) # Lemmatisation / stemmatisation
```

```{r load-file}

corpus <- read.delim("./data/2022-09-21_tweets_(#metoo)_(lang:fr_OR_lang:en).csv", sep = ",") %>% 
  mutate(user_id= as.character(user_id), id= as.character(id), retweeted_user_id= as.character(retweeted_user_id),
         retweeted_id= as.character(retweeted_id), quoted_id = as.character(quoted_id))

```

```{r retweeted-name-network}
retweet <- corpus %>% select(id, user_id, user_screen_name, retweeted_id, retweeted_user_id, retweeted_user, text, lang) %>%
  filter(!is.na(retweeted_id))
#%>% unnest_tokens(word, mentioned_names)


node1 <- retweet %>% select(user_id, user_screen_name) %>% rename(name = user_id, nom = user_screen_name) %>% distinct()
node2 <- retweet %>% select(retweeted_user_id, retweeted_user) %>% rename(name = retweeted_user_id, nom = retweeted_user) %>% distinct()
  
node <- node1 %>% bind_rows(node2) %>% distinct() 

edge <- retweet %>% rename(from = user_id, to = retweeted_user_id) %>% 
  mutate(from_name = user_screen_name, to_name = retweeted_user,
         loop = if_else(from==to, TRUE, FALSE)) %>% filter(loop==FALSE)

net <- tbl_graph(edges = edge, nodes = node) %N>%
  mutate(components = group_components())


n <- as_tibble(as_tbl_graph(net) %>% activate(nodes))  %>% 
  group_by(components) %>% count()

net <- tbl_graph(edges = edge, nodes = node, directed = F) %N>%
  mutate(components = group_components()) %>% filter(components == 1) %>%
  mutate(louvain = group_louvain()) 

n2 <- node %>% left_join(as_tibble(net %>% activate(nodes)), by = c("name","nom"))


net <- tbl_graph(edges = edge, nodes = n2) %N>%
  mutate(components = group_components()) %>% filter(components == 1) %>%
  mutate(degres_in= centrality_degree(mode ="in"),
         degres_out = centrality_degree(mode ="out"),
         degres = centrality_degree())

#net %>% ggraph('kk') + geom_edge_link(aes(color= lang)) + geom_node_point(aes(color=as.factor(louvain)))+theme_graph()


nodes <- as_tibble(net %>% activate(nodes)) 
edges <- as_tibble(net %>% activate(edges)) 


save(nodes, edges, file = "retweet_network.RData")


```






# Interactions directes

```{r}


corpus2 <- corpus %>% select(id, user_screen_name_x, user_name_x, to_username, date, text) %>% filter(to_username !="") %>% distinct()# %>% unnest_tokens(word, mentioned_names)

node1 <- corpus2 %>% select(user_screen_name_x) %>% rename(name = user_screen_name_x) %>% distinct() %>%
  left_join(df %>% rename(name = user_screen_name), by = "name") %>%
  mutate(annoted_account = if_else(is.na(Type_entite), FALSE, TRUE))

node2 <- corpus2 %>% select(to_username) %>% rename(name = to_username) %>% distinct() %>%
  left_join(df %>% rename(name = user_screen_name), by = "name") %>%
  mutate(annoted_account = if_else(is.na(Type_entite), FALSE, TRUE)) # %>% filter(corpus == 1)

node <- node1 %>% bind_rows(node2) %>% distinct()

edge <- corpus2 %>% rename(from = user_screen_name_x, to = to_username, user_name = user_name_x) %>% select(-id) %>% mutate(from_name = from, to_name = to)

net <- tbl_graph(edges = edge, nodes = node) %N>%   filter(annoted_account == TRUE) %E>%
  mutate(Year = as.numeric(format(as.Date(date, format="%Y-%m-%d"),"%Y")),
         user_world_from = .N()$User_world[from],
         user_world_to = .N()$User_world[to],
         user_world2_from = .N()$User_world2[from],
         user_world2_to = .N()$User_world2[to],
         user_logic_from = .N()$Synthetic_logics[from],
         user_logic_to = .N()$Synthetic_logics[to]
         ) %N>%
  mutate(degres_in = centrality_degree(mode = "in", normalized = T),
         degres_out = centrality_degree(mode = "out", normalized = T),
         degres = centrality_degree(mode = "all", normalized = T),
         degres2 = centrality_degree(mode = "all", normalized = F, loops = F)/(nrow(node)-1),
         degres_eig = centrality_eigen(directed = T),
         composante = group_components()) %E>%
  mutate(composante = .N()$composante[from]) %N>% filter(degres > 0)

net
subnet <- as_tbl_graph(simplify(net, remove.multiple = FALSE, remove.loops = TRUE))

subnet2 <-subnet %N>%  
  mutate(Year = as.numeric(format(as.Date(user_created_at, format="%Y-%m-%d"),"%Y")))  %E>% 
  #filter(Year == 2012) %N>% filter(Year <= 2012) %>%
  #convert(to_simple) %>%
  mutate(user_world_from = .N()$User_world[from],
         user_world_to = .N()$User_world[to],
         user_world2_from = .N()$User_world2[from],
         user_world2_to = .N()$User_world2[to],
         user_logic_from = .N()$Synthetic_logics[from],
         user_logic_to = .N()$Synthetic_logics[to]) %N>%
  mutate(degres_in = centrality_degree(mode = "in", normalized = F, loops = F),
         degres_out = centrality_degree(mode = "out", normalized = F, loops = F),
         degres = centrality_degree(mode = "all", normalized = T),
         degres2 = centrality_degree(mode = "all", normalized = F, loops = F)/(nrow(node)-1),
         degres_eig = centrality_eigen(directed = T),
         composante = group_components()) %E>%
  mutate(compo = .N()$composante[from])


nodes <- as_tibble(net %>% activate(nodes)) 
edges <- as_tibble(net %>% activate(edges)) 

subnodes <- as_tibble(subnet2 %>% activate(nodes)) 
subedges <- as_tibble(subnet2 %>% activate(edges)) 

save(nodes, edges, subnodes, subedges, file = "to_network.RData")

## Relation between user_role
n2 <- as_tibble(subnet2 %>% activate(nodes))  %>% 
  group_by(User_world) %>% summarise(n_nodes = n(), degres_in = sum(degres_in), degres_out = sum(degres_out)) %>% mutate(user_world_from = User_world, user_world_to = User_world) %>% select(-User_world)

e <- as_tibble(subnet2 %>% activate(edges)) 
Total <- nrow(as_tibble(subnet2 %>% activate(edges)))

e2 <- as_tibble(subnet2 %>% activate(edges)) %>% replace(is.na(.), "unknown") %>% filter(user_world_from !="", user_world_to !="") %>%
  group_by(user_world_from, user_world_to) %>% 
  summarize(weight = n()) %>%
  left_join((n2 %>% select(-user_world_to, -degres_in)), by ="user_world_from") %>%
  left_join((n2 %>% select(-user_world_from, -degres_out)), by ="user_world_to") %>%
  mutate(A = sum(weight),
         C = degres_in,
         B = (Total - A),
         D = (Total - C),
         AC = weight,
         AD = A-weight,
         BC = C - weight,
         BD = D - AD,
         norm_weight = weight/sum(weight)*100,
         norm_weight2 = weight/((n_nodes.x)*n_nodes.y)*100,
         norm_weight3 = (weight)/sqrt(degres_in*degres_out),
         phi = ((AC*BD)-(AD*BC))/sqrt(A*B*C*D))# %>% group_by(user_role_from) %>% mutate(ave_weight = mean(weight)) %>% filter(weight >= ave_weight)




matrice1 <- e2 %>% ggplot(aes(y = user_world_from, x = user_world_to, fill = weight))+
  geom_tile()+
  scale_fill_gradient(low = "white", high = "red") +
  coord_fixed()+
  labs(fill = "Valeurs absolues", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("a) Poids des liens")

matrice2 <- e2 %>% ggplot(aes(y = user_world_from, x = user_world_to, fill = norm_weight))+
  geom_tile()+
  scale_fill_gradient(low = "white", high = "red") +
  coord_fixed()+
  labs(fill = "Proportion", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("b) Proportion des liens")

matrice3 <- e2 %>% ggplot(aes(y = user_world_from, x = user_world_to, fill = norm_weight2))+
  geom_tile()+
  scale_fill_gradient(low = "white", high = "red") +
  coord_fixed()+
  labs(fill = "Proportion", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("b) Proportion des liens entrants")

matrice4 <- e2 %>% ggplot(aes(y = user_world_from, x = user_world_to, fill = phi))+
  geom_tile()+
  scale_fill_gradient(low = "blue", high = "red") +
  #scale_fill_brewer(palette = "BrBG")+
  coord_fixed()+
  labs(fill = "", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("c) Intensité des flux")
matrice4

matrice3 <- ggarrange(matrice1, matrice3, matrice4, nrow = 2, ncol = 2)
matrice3
ggsave("intensité_des_interactions.png", matrice3, device = "png", width = 30, height = 30, units = "cm")



```

```{r synthetic-logic}

n3 <- as_tibble(net %>% activate(nodes))
n3 <- as_tibble(subnet2 %>% activate(nodes))  %>% 
  group_by(Synthetic_logics) %>% summarise(n_nodes = n(), degres_in = sum(degres_in), degres_out = sum(degres_out)) %>% mutate(user_logic_from = Synthetic_logics, user_logic_to = Synthetic_logics) %>% select(-Synthetic_logics)

e1 <- as_tibble(subnet2 %>% activate(edges)) 
Total <- nrow(as_tibble(subnet2 %>% activate(edges)))

e3 <- as_tibble(subnet2 %>% activate(edges)) #%>% select(from, to, user_logic_from, user_world_from, user_world_to, user_logic_to) %>%
  left_join((n3 %>% select(name)), by ="user_logic_from")

e3 <- as_tibble(subnet2 %>% activate(edges)) %>% replace(is.na(.), "unknown") %>% filter(user_logic_from !="", user_logic_to !="") %>%
  group_by(user_logic_from, user_logic_to) %>% 
  summarize(weight = n()) %>%
  left_join((n3 %>% select(-user_logic_to, -degres_in)), by ="user_logic_from") %>%
  left_join((n3 %>% select(-user_logic_from, -degres_out)), by ="user_logic_to") %>%
  mutate(A = sum(weight),
         C = degres_in,
         B = (Total - A),
         D = (Total - C),
         AC = weight,
         AD = A-weight,
         BC = C - weight,
         BD = D - AD,
         norm_weight = weight/sum(weight)*100,
         norm_weight2 = weight/((n_nodes.x)*n_nodes.y)*100,
         norm_weight3 = (weight)/sqrt(degres_in*degres_out),
         phi = ((AC*BD)-(AD*BC))/sqrt(A*B*C*D))# %>% group_by(user_role_from) %>% mutate(ave_weight = mean(weight)) %>% filter(weight >= ave_weight)

matrice4 <- e3 %>% ggplot(aes(y = user_logic_from, x = user_logic_to, fill = phi))+
  geom_tile()+
  scale_fill_gradient(low = "blue", high = "red") +
  #scale_fill_brewer(palette = "BrBG")+
  coord_fixed()+
  labs(fill = "", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("c) Intensité des flux")
matrice4

```




```{r}

n <- as_tibble(net %>% activate(nodes))  %>% 
  group_by(composante) %>% summarise(n_nodes = n())
n

subcompo <- net %N>% filter(composante == 1) %>%
  mutate(Year = as.numeric(format(as.Date(user_creat, format="%Y-%m-%d"),"%Y")))  %E>%
  mutate(compo = .N()$composante[from])
subcompo

as_tibble(net %>% activate(edges)) %>% group_by(composante) %>% summarise(min_year = min(Year), max_year = max(Year), n = n())

e <- as_tibble(subcompo %>% activate(edges)) %>% group_by(Year, user_status_from) %>% summarise(n = n())

subcompo %>% ggraph()+geom_edge_link()+geom_node_point(aes(color = User_type)) + theme_graph()

corpus %>% filter(str_detect(links, "doi"))
```

```{r}

corpus1 <- corpus %>% select(anciennete, id, user_screen_name, user_name, mentioned_names, date, links, text, nb_of_biomarker) %>% unnest_tokens(word, mentioned_names)
corpus2 <- corpus %>% select(anciennete, id, user_screen_name, user_name, to_username, to_tweetid, date, links, text, nb_of_biomarker) %>% filter(to_username !="") %>% distinct()# %>% unnest_tokens(word, mentioned_names)


edge1 <- corpus1 %>% rename(from = user_screen_name, to = word) %>% 
  select(-anciennete, -links) %>% mutate(from_name = from, to_name = to, type_edge = "mention")
edge2 <- corpus2 %>% rename(from = user_screen_name, to = to_username) %>% 
  select(-anciennete, -links) %>% mutate(from_name = from, to_name = to, type_edge = "reply")

reduced_edge1 <- edge1 %>% left_join((edge2 %>% select(to_tweetid) %>% mutate(id = to_tweetid)), by ="id") %>%
  mutate(mentions_with_reply = if_else(is.na(to_tweetid), FALSE, TRUE))

edge <- reduced_edge1 %>% bind_rows(edge2) %>% select(-id, -to_tweetid) %>% relocate(to, .after = from) 

node1.1 <- edge %>% select(from) %>% rename(name = from) %>% distinct() %>%
  left_join(df %>% rename(name = user_scree), by = "name") %>%
  mutate(annoted_account = if_else(is.na(corpus), FALSE, TRUE))

node2.2 <- edge %>% select(to) %>% rename(name = to) %>% distinct() %>%
  left_join(df %>% rename(name = user_scree), by = "name") %>%
  mutate(annoted_account = if_else(is.na(corpus), FALSE, TRUE))

node <- node1.1 %>% bind_rows(node2.2) %>% distinct()

net <- tbl_graph(edges = edge, nodes = node) %N>%   filter(annoted_account == TRUE, corpus == 1) %E>%
  #filter(type_edge == "mention", mentions_with_reply == FALSE) %>%
  mutate(Year = as.numeric(format(as.Date(date, format="%Y-%m-%d"),"%Y")),
         user_role_from = .N()$User_role3[from],
         user_role_to = .N()$User_role3[to],
         user_status_from = .N()$User_status[from],
         user_status_to = .N()$User_status[to],
         user_status2_from = .N()$User_status2[from],
         user_status2_to = .N()$User_status2[to],
         user_status3_from = .N()$User_status3[from],
         user_status3_to = .N()$User_status3[to]) %N>%
  mutate(degres_in = centrality_degree(mode = "in", normalized = T),
         degres_out = centrality_degree(mode = "out", normalized = T),
         degres = centrality_degree(mode = "all", normalized = T),
         degres2 = centrality_degree(mode = "all", normalized = F, loops = F)/(nrow(node)-1),
         degres_eig = centrality_eigen(directed = T),
         composante = group_components()) %E>%
  mutate(composante = .N()$composante[from]) %N>% filter(degres > 0)

net
subnet <- as_tbl_graph(simplify(net))

subnet2 <- subnet %N>%  convert(to_simple) %>% #filter(composante == 1) %>%
  mutate(Year = as.numeric(format(as.Date(user_creat, format="%Y-%m-%d"),"%Y")))  %E>% 
  #filter(Year == 2012) %N>% filter(Year <= 2012) %>%
  #convert(to_simple) %>%
  mutate(user_role_from = .N()$User_role3[from],
         user_role_to = .N()$User_role3[to],
         user_status_from = .N()$User_status[from],
         user_status_to = .N()$User_status[to],
         user_status2_from = .N()$User_status2[from],
         user_status2_to = .N()$User_status2[to],
         user_status3_from = .N()$User_status3[from],
         user_status3_to = .N()$User_status3[to]) %N>%
  mutate(degres_in = centrality_degree(mode = "in", normalized = F, loops = F),
         degres_out = centrality_degree(mode = "out", normalized = F, loops = F),
         degres = centrality_degree(mode = "all", normalized = T),
         degres2 = centrality_degree(mode = "all", normalized = F, loops = F)/(nrow(node)-1),
         degres_eig = centrality_eigen(directed = T),
         composante = group_components()) %E>%
  mutate(compo = .N()$composante[from])

n2 <- as_tibble(subnet2 %>% activate(nodes))  %>% 
  group_by(User_status) %>% summarise(n_nodes = n(), degres_in = sum(degres_in), degres_out = sum(degres_out)) %>% mutate(user_status_from = User_status, user_status_to = User_status) %>% select(-User_status)

e <- as_tibble(subnet2 %>% activate(edges)) 
Total <- nrow(as_tibble(subnet2 %>% activate(edges)))

e2 <- as_tibble(subnet2 %>% activate(edges)) %>% replace(is.na(.), "unknown") %>%
  group_by(user_status_from, user_status_to) %>% 
  summarize(weight = n()) %>%
  left_join((n2 %>% select(-user_status_to, -degres_in)), by ="user_status_from") %>%
  left_join((n2 %>% select(-user_status_from, -degres_out)), by ="user_status_to") %>%
  mutate(norm_weight = weight/sum(weight)*100,
         norm_weight2 = weight/((n_nodes.x)*n_nodes.y)*100,
         norm_weight3 = (weight)/sqrt(degres_in*degres_out))# %>% group_by(user_role_from) %>% mutate(ave_weight = mean(weight)) %>% filter(weight >= ave_weight)



matrice4 <- e2 %>% ggplot(aes(y = user_status_from, x = user_status_to, fill = norm_weight3))+
  geom_tile()+
  scale_fill_gradient(low = "white", high = "red") +
  #scale_fill_brewer(palette = "BrBG")+
  coord_fixed()+
  labs(fill = "", y = "De", x = "À")+
  theme(axis.text.x = element_text(angle = 45, vjust = 0.9, hjust = 1),
        title = element_text(size = 12),
        legend.title = element_text(size = 11))+
  ggtitle("c) Intensité des flux")
matrice4


```

